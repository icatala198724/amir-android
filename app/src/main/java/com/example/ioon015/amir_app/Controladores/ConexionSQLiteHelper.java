package com.example.ioon015.amir_app.Controladores;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.ioon015.amir_app.Clases.AdapterList;
import com.example.ioon015.amir_app.Clases.Notificacion;

import java.util.ArrayList;


public class ConexionSQLiteHelper extends SQLiteOpenHelper {
    SQLiteDatabase db;
    ArrayList<Notificacion> notificaciones = new ArrayList<>();
    AdapterList adapterList;

    private static String CREAR_TABLA_NOTIFICACION = "CREATE TABLE " + Notificacion.TABLA_NOTIFICACION + " (" + Notificacion.CAMPO_TITULO + " TEXT,"
            + Notificacion.CAMPO_DESCRIPCION + " TEXT, " + Notificacion.CAMPO_FECHA + " TEXT," + Notificacion.CAMPO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT) ";


    public ConexionSQLiteHelper(Context context) {

        super(context, "notifications", null, 1);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREAR_TABLA_NOTIFICACION);

    }

    public void saveNotification(String title, String description) {
        db = super.getReadableDatabase();
        ContentValues values = new ContentValues();


        try {
            values.put(Notificacion.CAMPO_TITULO, title);
            values.put(Notificacion.CAMPO_DESCRIPCION, description);
        } catch (Exception error) {
            error.printStackTrace();
        }
        db.insert(Notificacion.TABLA_NOTIFICACION, null, values);
        db.close();


    }

    public void removeItem(int id) {
        db = this.getWritableDatabase();
        db.execSQL("delete from " + Notificacion.TABLA_NOTIFICACION + " where " + Notificacion.CAMPO_ID + " = " + id);

        db.close();
    }


    public ArrayList<Notificacion> callAll() {
        db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("select * from " + Notificacion.TABLA_NOTIFICACION, null);
        if (cursor.moveToFirst()) {

            for (int i = 0; i < cursor.getString(0).length(); i++) {

                Notificacion contenedor = new Notificacion(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3));
                notificaciones.add(contenedor);

            }
        }

        db.close();

        return notificaciones;
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int viejaVersion, int nuevaVersion) {

        db.execSQL("DROP TABLE IF EXISTS notifications");

        db.execSQL(CREAR_TABLA_NOTIFICACION);

    }

    public ArrayList insertOnRv() {
        ArrayList<Notificacion> list = new ArrayList<>();
        SQLiteDatabase database = this.getWritableDatabase();
        String q = "SELECT * from notifications";
        Cursor registros = database.rawQuery(q, null);
        if (registros.moveToFirst()) {
            do {
                list.add(new Notificacion(registros.getString(0), registros.getString(1), registros.getString(2), registros.getString(3)));

            } while (registros.moveToNext());
        }

        return list;
    }


}



