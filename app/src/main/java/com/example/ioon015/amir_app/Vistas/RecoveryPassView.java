package com.example.ioon015.amir_app.Vistas;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.ioon015.amir_app.Controladores.VolleyCallback;
import com.example.ioon015.amir_app.Peticiones.LoadData;
import com.example.ioon015.amir_app.R;

public class RecoveryPassView extends AppCompatActivity {

    Button btnRecovery;
    ProgressBar progressBar;
    FrameLayout frameLayout;

    EditText txtRecoveryUser;

    LoadData loadData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recovery_pass_view);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        btnRecovery = findViewById(R.id.btnRecovery);
        progressBar = findViewById(R.id.progressBarRecovery);
        frameLayout = findViewById(R.id.frameLayoutProgressRecovery);
        txtRecoveryUser = findViewById(R.id.txtRecoveryUser);

        progressBar.setVisibility(View.GONE);
        frameLayout.setVisibility(View.GONE);

        loadData = new LoadData(this);

        btnRecovery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressBar.setIndeterminate(true);
                progressBar.setVisibility(View.VISIBLE);
                frameLayout.setVisibility(View.VISIBLE);

                String email = txtRecoveryUser.getText().toString();

                loadData.guessAccess(email, new VolleyCallback() {
                    @Override
                    public void onSuccess(Object object) {

                        progressBar.setVisibility(View.GONE);
                        frameLayout.setVisibility(View.GONE);

                        boolean response = Boolean.parseBoolean((String) object);

                        if(response){
                            Toast.makeText(getApplicationContext(), "Se ha enviado una contraseña nueva a su email", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), LoginView.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }

                    }
                });

            }
        });


    }


}
