package com.example.ioon015.amir_app.Controladores;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.ioon015.amir_app.Clases.FlavourTable;
import com.example.ioon015.amir_app.Clases.TokenTable;
import com.example.ioon015.amir_app.Clases.UserInfo;
import com.example.ioon015.amir_app.Clases.UserTable;
import com.example.ioon015.amir_app.Peticiones.LoadData;
import com.example.ioon015.amir_app.Vistas.LoginView;
import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DataBaseController extends SQLiteOpenHelper {

    SQLiteDatabase db;
    Context context;
    private static final String MIR_CODE_FLAVOUR = "MIR";

    public DataBaseController(Context context) {
        super(context, "UserData", null, 4);
        this.context = context;
    }


    public void setToken(String token){

        removeToken();

        db = super.getWritableDatabase();

        Gson gson = new Gson();
        TokenTable tokenValues = gson.fromJson(token, TokenTable.class);

        ContentValues data = new ContentValues();

        data.put(TokenTable.ACCESS_TOKEN , tokenValues.getAccess_token() );
        data.put(TokenTable.TOKEN_TYPE , tokenValues.getToken_type() );
        data.put(TokenTable.REFRESH_TOKEN , tokenValues.getRefresh_token() );
        data.put(TokenTable.EXPIRES_IN , tokenValues.getExpires_in() );
        data.put(TokenTable.SCOPE , tokenValues.getScope() );

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format1 = new SimpleDateFormat("HH:mm dd-MM-yyyy");
        String tiempo = format1.format(calendar.getTime());

        data.put(TokenTable.DATE_TOKEN, tiempo);

        db.insert(TokenTable.TABLE_NAME, null, data);

        db.close();

    }

    public String getToken(){

        String token = "";
        String dateToken = "";

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery("Select " + TokenTable.ACCESS_TOKEN + ", " + TokenTable.DATE_TOKEN + " from " + TokenTable.TABLE_NAME, null);

        if(cursor.moveToFirst()){
            token = cursor.getString(0);
            dateToken = cursor.getString(1);
        }



        /*try {

            Calendar calendar = Calendar.getInstance();
            Date dateNow = calendar.getTime();

            calendar.add(Calendar.DAY_OF_YEAR, 6);
            SimpleDateFormat formatter = new SimpleDateFormat("HH:mm dd-MM-yyyy");
            String fechaMaxima = formatter.format(calendar.getTime());

            Date dateMax = formatter.parse(fechaMaxima);

            if(dateNow.after(dateMax)){

                db.execSQL("DELETE FROM " + TokenTable.TABLE_NAME);
                LoadData loadData = new LoadData(context);
                String response =  getLoginData();
                String user = response.split("=")[0];
                String pass = response.split("=")[1];

                loadData.login(user, pass, new VolleyCallback() {
                    @Override
                    public void onSuccess(Object object) {
                        boolean respuesta = (boolean) object;

                        if(respuesta){
                            token = getToken();
                        }else{
                            Intent intent = new Intent(context, LoginView.class);
                            context.startActivity(intent);
                        }

                    }
                });
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }*/

        db.close();

        return token;
    }

    public void saveLoginData(String email, String password){

        db = this.getWritableDatabase();

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format1 = new SimpleDateFormat("HH:mm dd-MM-yyyy");
        String dateFormated = format1.format(calendar.getTime());

        ContentValues loginData = new ContentValues();

        loginData.put("email", email);
        loginData.put("password", password);
        loginData.put("date", dateFormated);

        db.execSQL("INSERT INTO datosusuario(email, password, date) VALUES('" + email + "', '" + password + "', '" + dateFormated + "')");

        db.close();

    }

    public String getLoginData(){

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("Select email, password, date from " + " datosusuario", null);

        String email = "";
        String password = "";
        String date = "";

        if(cursor.moveToFirst()){
            email = cursor.getString(0);
            password = cursor.getString(1);
            date = cursor.getString(2);
        }

        db.close();

        return email + "=" + password + "=" + date;
    }


    public boolean existToken(){

        boolean tokenExist = false;

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("Select " + TokenTable.ACCESS_TOKEN + " from " + TokenTable.TABLE_NAME, null);

        if(cursor.moveToFirst()){
            String token = cursor.getString(0);

            if(token != null){
                tokenExist = true;
            }else{
                tokenExist = false;
            }
        }

        db.close();

        return tokenExist;

    }

    public void saveUserInfo(String userInfo){

        db = this.getWritableDatabase();
        Gson gson = new Gson();
        UserInfo[] userInfos = gson.fromJson(userInfo, UserInfo[].class);

        for (int i=0; i<userInfos.length; i++){
            if(userInfos[i].getFlavor().getCode().equals(MIR_CODE_FLAVOUR)){

                ContentValues dataFlavour = new ContentValues();

                dataFlavour.put(FlavourTable.CODE , userInfos[i].getFlavor().getCode() );
                dataFlavour.put(FlavourTable.BASE_URL , userInfos[i].getFlavor().getBaseUrl() );
                dataFlavour.put(FlavourTable.DESCRIPTION , userInfos[i].getFlavor().getDescription() );

                //{"codigo":"555", "nombre: "Alehjandro"}

                db.insert(FlavourTable.TABLE_NAME, null, dataFlavour);

                ContentValues dataUser = new ContentValues();

                dataUser.put(UserTable.NAME , userInfos[i].getUser().getName() );
                dataUser.put(UserTable.SURNAME , userInfos[i].getUser().getSurname() );
                dataUser.put(UserTable.EMAIL , userInfos[i].getUser().getEmail() );
                dataUser.put(UserTable.USER_NAME , userInfos[i].getUser().getUserName() );
                dataUser.put(UserTable.ACTIVE , userInfos[i].getUser().getActive() );
                dataUser.put(UserTable.CATEGORY , userInfos[i].getUser().getCategory() );
                dataUser.put(UserTable.ID_COUNTTRY , userInfos[i].getUser().getIdCountry() );
                dataUser.put(UserTable.ID_CURSO , userInfos[i].getUser().getIdCurso() );
                dataUser.put(UserTable.ROLE , userInfos[i].getUser().getRole() );

                db.insert(UserTable.TABLE_NAME, null, dataUser);

            }

        }

        db.close();

    }

    public UserInfo getUserInfo(){

        UserInfo userInfo = new UserInfo();

        SQLiteDatabase db = this.getReadableDatabase();

        FlavourTable flavourValues = new FlavourTable();

        Cursor cursorFlavourTable = db.rawQuery("Select * from " + FlavourTable.TABLE_NAME, null);

        if(cursorFlavourTable.moveToFirst()){

            for(int i=0; i<cursorFlavourTable.getColumnNames().length; i++){
                flavourValues.setCode(cursorFlavourTable.getString(0));
                flavourValues.setBaseUrl(cursorFlavourTable.getString(1));
                flavourValues.setDescription(cursorFlavourTable.getString(2));
            }
        }

        UserTable userValues = new UserTable();
        Cursor cursorUserTable = db.rawQuery("Select * from " + UserTable.TABLE_NAME, null);

        if(cursorUserTable.moveToFirst()){

            for(int i=0; i<cursorUserTable.getColumnNames().length; i++){
                userValues.setName(cursorUserTable.getString(0));
                userValues.setSurname(cursorUserTable.getString(1));
                userValues.setUserName(cursorUserTable.getString(2));
                userValues.setEmail(cursorUserTable.getString(3));
                userValues.setRole(cursorUserTable.getString(4));
                userValues.setCategory(cursorUserTable.getString(5));
                userValues.setActive(cursorUserTable.getString(6));
                userValues.setIdCurso(cursorUserTable.getString(7));
                userValues.setIdCountry(cursorUserTable.getString(8));
            }
        }


        db.close();

        userInfo.setFlavor(flavourValues);
        userInfo.setUser(userValues);

        return userInfo;
    }

    public void notShowSlides(boolean show){

        db = this.getWritableDatabase();
        db.execSQL("DELETE FROM slidesdata");
        db.execSQL("INSERT INTO slidesdata(show) VALUES('" + show + "')");
        db.close();

    }

    public boolean showSlides(){

        db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("Select show from slidesdata", null);

        boolean show = false;

        if(cursor.moveToFirst()){
            show = Boolean.parseBoolean(cursor.getString(0));
            Log.d("APP", "------------------------------------------- Mostrar slides: " + show);
        }

        db.close();

        return show;
    }

    public void removeToken(){

        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("delete from "+ TokenTable.TABLE_NAME);
        db.execSQL("delete from "+ UserTable.TABLE_NAME);
        db.execSQL("delete from "+ FlavourTable.TABLE_NAME);
        db.execSQL("delete from datosusuario");

        db.close();

    }


    /*************************************************************************************************************************/

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE slidesdata(show TEXT)");
        db.execSQL("INSERT INTO slidesdata(show) VALUES('true')");

        db.execSQL("CREATE TABLE " + TokenTable.TABLE_NAME + "(" +
                TokenTable.ACCESS_TOKEN + " TEXT, " +
                TokenTable.TOKEN_TYPE + " TEXT ," +
                TokenTable.REFRESH_TOKEN + " TEXT, " +
                TokenTable.EXPIRES_IN + " TEXT," +
                TokenTable.SCOPE + " TEXT," +
                TokenTable.DATE_TOKEN + " TEXT)");

        db.execSQL("CREATE TABLE " + FlavourTable.TABLE_NAME + "(" +
                FlavourTable.CODE + " TEXT, " +
                FlavourTable.DESCRIPTION + " TEXT," +
                FlavourTable.BASE_URL + " TEXT)");

        db.execSQL("CREATE TABLE " + UserTable.TABLE_NAME + "(" +
                UserTable.NAME + " TEXT, " +
                UserTable.SURNAME + " TEXT," +
                UserTable.USER_NAME + " TEXT, " +
                UserTable.EMAIL + " TEXT," +
                UserTable.ROLE + " TEXT," +
                UserTable.CATEGORY + " TEXT," +
                UserTable.ACTIVE + " TEXT," +
                UserTable.ID_CURSO + " TEXT," +
                UserTable.ID_COUNTTRY + " TEXT)");

        db.execSQL("CREATE TABLE datosusuario(email TEXT, password TEXT, date TEXT)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("drop table if exists slidesdata");
        db.execSQL("drop table if exists datosusuario");
        db.execSQL("drop table if exists " + TokenTable.TABLE_NAME);
        db.execSQL("drop table if exists " + FlavourTable.TABLE_NAME);
        db.execSQL("drop table if exists " + UserTable.TABLE_NAME);

        db.execSQL("CREATE TABLE slidesdata(show TEXT)");
        db.execSQL("INSERT INTO slidesdata(show) VALUES('true')");

        db.execSQL("CREATE TABLE " + TokenTable.TABLE_NAME + "(" +
                TokenTable.ACCESS_TOKEN + " TEXT, " +
                TokenTable.TOKEN_TYPE + " TEXT ," +
                TokenTable.REFRESH_TOKEN + " TEXT, " +
                TokenTable.EXPIRES_IN + " TEXT," +
                TokenTable.SCOPE + " TEXT," +
                TokenTable.DATE_TOKEN + " TEXT)");

        db.execSQL("CREATE TABLE " + FlavourTable.TABLE_NAME + "(" +
                FlavourTable.CODE + " TEXT, " +
                FlavourTable.DESCRIPTION + " TEXT," +
                FlavourTable.BASE_URL + " TEXT)");

        db.execSQL("CREATE TABLE " + UserTable.TABLE_NAME + "(" +
                UserTable.NAME + " TEXT, " +
                UserTable.SURNAME + " TEXT," +
                UserTable.USER_NAME + " TEXT, " +
                UserTable.EMAIL + " TEXT," +
                UserTable.ROLE + " TEXT," +
                UserTable.CATEGORY + " TEXT," +
                UserTable.ACTIVE + " TEXT," +
                UserTable.ID_CURSO + " TEXT," +
                UserTable.ID_COUNTTRY + " TEXT)");

        db.execSQL("CREATE TABLE datosusuario(email TEXT, password TEXT, date TEXT)");
    }








}
