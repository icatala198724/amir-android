package com.example.ioon015.amir_app.Clases;

import java.io.Serializable;

public class BeaconData implements Serializable {

    public String tag;
    public String id;

    public BeaconData(String tag, String id) {
        this.tag = tag;
        this.id = id;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
