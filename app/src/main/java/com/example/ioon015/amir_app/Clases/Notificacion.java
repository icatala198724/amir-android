package com.example.ioon015.amir_app.Clases;

public class Notificacion {

    public String title;
    public String description;
    public String date;
    public String id;

    public static String TABLA_NOTIFICACION = "notifications";
    public static String CAMPO_TITULO = "title";
    public static String CAMPO_DESCRIPCION = "description";
    public static String CAMPO_FECHA = "date";
    public static String CAMPO_ID = "id";

    public Notificacion(String title, String description, String date, String id) {
        this.title = title;
        this.description = description;
        this.date = date;
        this.id = id;
    }

    public String getTitle() {

        return title;
    }

    public void setTitle(String title) {

        this.title = title;
    }

    public String getDescription() {

        return description;
    }

    public void setDescription(String description) {

        this.description = description;
    }

    public String getDate() {

        return date;
    }

    public void setDate(String date) {

        this.date = date;
    }

    public String getId() {

        return id;
    }

    public void setId(String id) {

        this.id = id;
    }
}
