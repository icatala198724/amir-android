package com.example.ioon015.amir_app.Clases;

public class FlavourTable {
    public static final String TABLE_NAME = "flavor";
    public static final String CODE = "code";
    public static final String DESCRIPTION = "description";
    public static final String BASE_URL = "baseUrl";

    String code;
    String description;
    String baseUrl;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }
}
