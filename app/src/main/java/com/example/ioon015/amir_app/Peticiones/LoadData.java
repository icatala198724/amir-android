package com.example.ioon015.amir_app.Peticiones;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.ioon015.amir_app.Clases.Classroom;
import com.example.ioon015.amir_app.Clases.Event;
import com.example.ioon015.amir_app.Controladores.DataBaseController;
import com.example.ioon015.amir_app.Controladores.VolleyCallback;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class LoadData {

    private Context context;
    private String refreshToken = "";

    private final String URL_LOGIN = "http://login.academiamir.com/amir-central/oauth/token";
    private final String URL_USER_INFO = "http://login.academiamir.com/amir-central/api/account/userinfo";
    private final String URL_RECOVERY_PASS = "http://login.academiamir.com/amir-central/api/account/passwordRecoveryRequest";
    private final String URL_NEXT_EVENTS = "http://login.academiamir.com/amir-central/api/events/next";
    private final String URL_TODAY_EVENTS = "http://login.academiamir.com/amir-central/api/events/today";
    private final String URL_CONFIRM_PRESENCE = "http://login.academiamir.com/amir-central/api/events/";
    private final String URL_CHECK_PARTICIPATION = "http://login.academiamir.com/amir-central/api/events/";
    private final String URL_CLASSROOM = "http://login.academiamir.com/amir-central/api/classrooms/";

    private DataBaseController dataBaseController;
    private Gson gson = new Gson();

    public LoadData(Context context) {
        this.context = context;
    }


    public void login(final String user, final String pass, final VolleyCallback volleyCallback) {

        dataBaseController = new DataBaseController(context);

        RequestQueue queue = Volley.newRequestQueue(context);

        String parametros = "?grant_type=password&username=" + user + "&password=" + pass;

        StringRequest sr = new StringRequest(Request.Method.POST, URL_LOGIN + parametros, new Response.Listener<String>() {
            @Override
            public void onResponse(String token_login) {
                //dataBaseController.removeToken();
                userInfo(token_login);
                dataBaseController.setToken(token_login);
                dataBaseController.saveLoginData(user, pass);
                volleyCallback.onSuccess(true);

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                volleyCallback.onSuccess(false);
            }
        }) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                /*Headers*/
                headers.put("Accept", "application/json");
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Basic aW9vbi1ldmVudHM6czBFMl5vMXlmJml1");

                return headers;
            }

        };
        queue.add(sr);
    }


    public void userInfo(final String token) {

        dataBaseController = new DataBaseController(context);

        RequestQueue queue = Volley.newRequestQueue(context);

        StringRequest sr = new StringRequest(Request.Method.GET, URL_USER_INFO, new Response.Listener<String>() {
            @Override
            public void onResponse(String userInfo) {
                dataBaseController.saveUserInfo(userInfo);
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "No se han podido cargar los datos del usuario", Toast.LENGTH_LONG).show();

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                /*Headers*/

                try {
                    JSONObject jsonObject = new JSONObject(token);
                    String tokenValue = jsonObject.get("access_token").toString();
                    headers.put("Authorization", "Bearer " + tokenValue);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return headers;
            }

        };
        queue.add(sr);
    }

    public void guessAccess(final String email, final VolleyCallback volleyCallback) {

        RequestQueue queue = Volley.newRequestQueue(context);

        String parametros = "?grant_type=client_credentials";

        StringRequest sr = new StringRequest(Request.Method.POST, URL_LOGIN + parametros, new Response.Listener<String>() {
            @Override
            public void onResponse(String responseRecovery) {

                GuessToken guessToken = gson.fromJson(responseRecovery, GuessToken.class);

                recoveryPassword(email, guessToken.getAccess_token());

                volleyCallback.onSuccess(true);

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "No se ha podido realizar la solicitud", Toast.LENGTH_LONG).show();
                volleyCallback.onSuccess(false);
            }
        }) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();

                /*Headers*/
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Basic aW9vbi1ldmVudHM6czBFMl5vMXlmJml1");

                return headers;
            }

        };
        queue.add(sr);

    }


    public void recoveryPassword(final String email, final String guessToken) {

        RequestQueue queue = Volley.newRequestQueue(context);

        String parametros = "?email=" + email;

        StringRequest sr = new StringRequest(Request.Method.POST, URL_RECOVERY_PASS + parametros, new Response.Listener<String>() {
            @Override
            public void onResponse(String responseRecovery) {
                Toast.makeText(context, "Solicitud de nueva contraseña realizada correctamente", Toast.LENGTH_LONG).show();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "No se ha podido realizar la solicitud", Toast.LENGTH_LONG).show();
            }
        }) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                /*Headers*/
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Bearer " + guessToken);

                return headers;
            }

        };
        queue.add(sr);

    }


    public void nextEvents(boolean allEvents, final VolleyCallback volleyCallback) {

        dataBaseController = new DataBaseController(context);
        final String token = dataBaseController.getToken();


        RequestQueue queue = Volley.newRequestQueue(context);
        String url_data = "";

        if (allEvents) {
            url_data = URL_NEXT_EVENTS;
        } else {
            url_data = URL_TODAY_EVENTS;
        }

        StringRequest sr = new StringRequest(Request.Method.GET, url_data, new Response.Listener<String>() {
            @Override
            public void onResponse(String responseRecovery) {

                JsonParser jsonParser = new JsonParser();
                JsonArray jsonArray = (JsonArray) jsonParser.parse(responseRecovery);
                Event[] events = new Event[jsonArray.size()];

                for (int i = 0; i < jsonArray.size(); i++) {
                    events[i] = gson.fromJson(jsonArray.get(i), Event.class);
                }

                volleyCallback.onSuccess(events);

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "No se ha podido cargar las clases", Toast.LENGTH_LONG).show();

                volleyCallback.onSuccess("false");
            }
        }) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                //Headers
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Bearer " + token);

                return headers;
            }

        };
        queue.add(sr);

    }

    public void listClassroom(final VolleyCallback volleyCallback) {

        dataBaseController = new DataBaseController(context);
        final String token = dataBaseController.getToken();


        RequestQueue queue = Volley.newRequestQueue(context);


        StringRequest sr = new StringRequest(Request.Method.GET, URL_CLASSROOM, new Response.Listener<String>() {
            @Override
            public void onResponse(String responseRecovery) {

                JsonParser jsonParser = new JsonParser();
                JsonArray jsonArray = (JsonArray) jsonParser.parse(responseRecovery);
                Classroom[] classrooms = new Classroom[jsonArray.size()];

                for (int i = 0; i < jsonArray.size(); i++) {
                    classrooms[i] = gson.fromJson(jsonArray.get(i), Classroom.class);

                    Log.d("APP", ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Datos: " + jsonArray);
                }

                volleyCallback.onSuccess(classrooms);

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "No se ha podido cargar las clases", Toast.LENGTH_LONG).show();

                volleyCallback.onSuccess(false);
            }
        }) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                /*Headers*/
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Bearer " + token);

                return headers;
            }

        };
        queue.add(sr);

    }


    public void checkParticipation(String flavorCode, String eventId, final VolleyCallback volleyCallback){

        final String token = dataBaseController.getToken();

        RequestQueue queue = Volley.newRequestQueue(context);

        StringRequest sr = new StringRequest(Request.Method.GET, URL_CHECK_PARTICIPATION + flavorCode + "-" + eventId + "/checkParticipation", new Response.Listener<String>() {
            @Override
            public void onResponse(String responseRecovery) {

                Log.d("APP", "............................................... RESPUESTA DE CHECKPARTICIPATION: " + responseRecovery);

                JSONObject jsonObject = null;
                String status = "";

                try {
                    jsonObject = new JSONObject(responseRecovery);
                    status = jsonObject.getString("status");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                String value = "";

                /*****************************************************************/
                if(status.equals("CONFIRMED")){
                    value = "true";
                }else{
                    value = "false";
                }

                volleyCallback.onSuccess(value);

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "No se ha podido comprobar la confirmacion", Toast.LENGTH_LONG).show();
                Log.d("APP", "............................................... Error: " + error);
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                /*Headers*/
                headers.put("Authorization", "Bearer " + token);

                //headers.put()

                return headers;
            }

        };

        queue.add(sr);


    }


    class GuessToken {
        String access_token;
        String token_type;
        String expires_in;
        String scope;

        public GuessToken(String access_token, String token_type, String expires_in, String scope) {
            this.access_token = access_token;
            this.token_type = token_type;
            this.expires_in = expires_in;
            this.scope = scope;
        }

        public String getAccess_token() {
            return access_token;
        }

        public void setAccess_token(String access_token) {
            this.access_token = access_token;
        }

        public String getToken_type() {
            return token_type;
        }

        public void setToken_type(String token_type) {
            this.token_type = token_type;
        }

        public String getExpires_in() {
            return expires_in;
        }

        public void setExpires_in(String expires_in) {
            this.expires_in = expires_in;
        }

        public String getScope() {
            return scope;
        }

        public void setScope(String scope) {
            this.scope = scope;
        }
    }

}
