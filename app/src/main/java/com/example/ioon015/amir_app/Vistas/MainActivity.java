package com.example.ioon015.amir_app.Vistas;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.ioon015.amir_app.Controladores.DataBaseController;
import com.example.ioon015.amir_app.Controladores.VolleyCallback;
import com.example.ioon015.amir_app.Peticiones.LoadData;
import com.example.ioon015.amir_app.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    DataBaseController dataBaseController;
    LoadData loadData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        dataBaseController = new DataBaseController(this);
        loadData = new LoadData(getBaseContext());

        boolean tokenExist = dataBaseController.existToken();

        if(tokenExist){

            final String loginData = dataBaseController.getLoginData();

            try {
                SimpleDateFormat format = new SimpleDateFormat("HH:mm dd-MM-yyyy");
                Calendar calendar = Calendar.getInstance();
                Date dateToday = format.parse(format.format(calendar.getTime()));
                calendar.setTime(format.parse(loginData.split("=")[2]));
                calendar.add(Calendar.DATE, 5);

                Date dateEndToken = calendar.getTime();

                if(dateToday.after(dateEndToken)){

                    loadData.login(loginData.split("=")[0], loginData.split("=")[1], new VolleyCallback() {
                        @Override
                        public void onSuccess(Object object) {

                            boolean response = (boolean) object;

                            if(response){
                                dataBaseController.saveLoginData(loginData.split("=")[0], loginData.split("=")[1]);

                                Intent intent = new Intent(getApplicationContext(), HomeView.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }else{

                                Intent intent = new Intent(getApplicationContext(), LoginView.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }

                        }
                    });
                }else{

                    Intent intent = new Intent(getApplicationContext(), HomeView.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }



        }else{

            Intent intent = new Intent(this, LoginView.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }

        finish();

    }





}
