package com.example.ioon015.amir_app.Peticiones;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.ioon015.amir_app.Controladores.DataBaseController;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class SendPost extends AsyncTask {

    private final String URL_CONFIRM_PRESENCE = "http://login.academiamir.com/amir-central/api/events/";

    String flavorCode;
    String eventId;
    String token;
    Context context;
    String response;

    public SendPost(Context context, String flavorCode, String eventId) {
        this.context = context;
        this.flavorCode = flavorCode;
        this.eventId = eventId;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();


        DataBaseController dataBaseController = new DataBaseController(context);
        token = dataBaseController.getToken();
    }

    @Override
    protected Object doInBackground(Object[] objects) {

        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        try {
            URL url = new URL(URL_CONFIRM_PRESENCE + flavorCode + "-" + eventId + "/confirmParticipation");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Authorization", "Bearer " + token);
            conn.setDoOutput(true);
            conn.setDoInput(true);


            JSONObject jsonParam = new JSONObject();


            jsonParam.put("confirmTime", null);
            jsonParam.put("classCode", null);


            Log.i("JSON", ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Value: " + jsonParam.toString());
            DataOutputStream os = new DataOutputStream(conn.getOutputStream());
            //os.writeBytes(URLEncoder.encode(jsonParam.toString(), "UTF-8"));
            os.writeBytes(jsonParam.toString());


            os.flush();
            os.close();

            /*OK*/
            response = conn.getResponseMessage();
            Log.i("APP", "**********************" + String.valueOf(conn.getResponseCode()));
            Log.i("APP" , "***********************" + conn.getResponseMessage());

            conn.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    Log.e("APP", "Error closing stream", e);
                }
            }
        }

        boolean result = false;

        if(response.equals("")){
            result = true;
        }else{
            result = false;
        }

        return result;
    }


}
