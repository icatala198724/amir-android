package com.example.ioon015.amir_app.Vistas;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.ioon015.amir_app.Controladores.DataBaseController;
import com.example.ioon015.amir_app.R;

public class Settings extends Fragment {

    private Button btnSalir;
    private View view;
    private Button btnSlidesHelper;
    private Button btnDeleteData;

    private DataBaseController dataBaseController;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.activity_settings, container, false);

        dataBaseController = new DataBaseController(getContext());

        //btnSalir = new Button(getContext());
        btnSalir = view.findViewById(R.id.btnSalir);
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DataBaseController dataBaseController = new DataBaseController(getContext());
                dataBaseController.removeToken();

                Intent intent = new Intent(getContext(), LoginView.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        btnSlidesHelper = view.findViewById(R.id.btnSlideHelper);
        btnSlidesHelper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), SlidesGuide.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        btnDeleteData = view.findViewById(R.id.btnDeleteData);
        btnDeleteData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmationDelete();

            }
        });

        return view;
    }

    public void confirmationDelete(){

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        builder.setTitle("Atención")
                .setMessage("¿Desea eliminar toda la información?\n\nVolverá al inicio de la aplicación")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        //Toast.makeText(getContext(), "Has eliminado tu informacion", Toast.LENGTH_SHORT).show();
                        dataBaseController.removeToken();
                        Intent intent = new Intent(getContext(), LoginView.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        Toast.makeText(getContext(), "No se ha realizado la acción", Toast.LENGTH_SHORT).show();

                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();

    }
}
