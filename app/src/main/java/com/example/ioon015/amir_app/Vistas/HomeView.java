package com.example.ioon015.amir_app.Vistas;


import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.example.ioon015.amir_app.Clases.Event;
import com.example.ioon015.amir_app.Controladores.DataBaseController;
import com.example.ioon015.amir_app.R;

public class HomeView extends AppCompatActivity {


    ActionBar toolbar;

    BottomNavigationView bottomNavigationView;

    FrameLayout frameNoNotifications;

    BottomNavigationItemView btnMnItemNotifications;

    Fragment fragment = null;
    int count = 0;

    DataBaseController dataBaseController;
    private static final String EVENT_IN_TIME = "EVENT_IN_TIME";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        dataBaseController = new DataBaseController(this);

        toolbar = getSupportActionBar();
        frameNoNotifications = findViewById(R.id.frameNoNotifications);
        btnMnItemNotifications = findViewById(R.id.mnItemSettings);
        bottomNavigationView = new BottomNavigationView(this);
        bottomNavigationView = findViewById(R.id.bottomMenu);

        loadFragment(new Classes());

        bottomNavigationView.setSelectedItemId(R.id.mnItemClasses);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.mnItemSettings:
                        fragment = new Settings();
                        /*Intent intentSettings = new Intent(getApplicationContext(), Settings.class);
                        startActivity(intentSettings);*/
                        break;
                    /*case R.id.mnItemClasses:
                        fragment = new Classes();
                        break;*/
                    case R.id.mnItemNotifications:
                        fragment = new Notifications();
                        break;
                }

                return loadFragment(fragment);
            }

        });

    }




    @Override
    public void onBackPressed() {

        if(fragment != null){

            if(fragment.toString().contains("Notifications") || fragment.toString().contains("Settings")){
                count = 0;
                fragment = new Classes();
                loadFragment(new Classes());

            }else {

                if (fragment.toString().contains("Classes")){

                    if(count == 0){
                        Toast.makeText(getApplicationContext(), "Pulsa de nuevo para salir", Toast.LENGTH_SHORT).show();
                        count++;
                    }else{
                        super.onBackPressed();
                        getFragmentManager().popBackStack();
                    }

                }
            }

        }else{
            Toast.makeText(getApplicationContext(), "Pulsa de nuevo para salir", Toast.LENGTH_SHORT).show();
            fragment = new Classes();
        }

    }

    private boolean loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_Container, fragment)
                    .commit();

            if(fragment.toString().contains("Classes")){
                bottomNavigationView.setSelectedItemId(R.id.mnItemClasses);
            }

            return true;
        }
        return false;
    }


    public void startBeaconConfirmation(Event event){
        Bundle bundle = new Bundle();
        //bundle.putSerializable(EVENT_IN_TIME, event);

        //Toast.makeText(getApplicationContext(), "Tienes una clase en hora", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this.getApplicationContext(), PresenceConfirmation.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}
