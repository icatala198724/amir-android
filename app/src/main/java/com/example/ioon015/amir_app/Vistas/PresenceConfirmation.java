package com.example.ioon015.amir_app.Vistas;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.estimote.mustard.rx_goodness.rx_requirements_wizard.Requirement;
import com.estimote.mustard.rx_goodness.rx_requirements_wizard.RequirementsWizardFactory;
import com.example.ioon015.amir_app.Clases.Classroom;
import com.example.ioon015.amir_app.Clases.Event;
import com.example.ioon015.amir_app.Controladores.ProximityContentManager;
import com.example.ioon015.amir_app.Controladores.VolleyCallback;
import com.example.ioon015.amir_app.Peticiones.LoadData;
import com.example.ioon015.amir_app.Peticiones.SendPost;
import com.example.ioon015.amir_app.R;

import java.util.List;
import java.util.concurrent.ExecutionException;

import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.INTERNET;

public class PresenceConfirmation extends AppCompatActivity {

    private static final String EVENT_IN_TIME = "EVENT_IN_TIME";
    TextView txtClassroomConfirmation;
    TextView txtTimeConfirmation;
    TextView txtStatusConfirmation;
    TextView txtConecting;
    ProgressBar pgbConfirmation;
    ImageView imgConfirmation;
    boolean located = false;
    private int PERMISSION_REQUEST_CODE = 321;

    Event event;
    LoadData loadData;
    ProximityContentManager proximityContentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_presence_confirmation);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        loadData = new LoadData(this);
        Bundle bundle = getIntent().getExtras();

        event = new Event(bundle.get("EventValue0").toString(), bundle.get("EventValue1").toString(), new Classroom("", "", bundle.get("EventValue2").toString(), "", bundle.get("Latitude").toString(), bundle.get("Longitude").toString()), bundle.get("EventValue3").toString(), bundle.get("EventValue4").toString(), bundle.get("EventValue5").toString());

        activateServices();

        imgConfirmation = findViewById(R.id.imgConfirmation);
        pgbConfirmation = findViewById(R.id.pgbConfirmation);
        txtConecting = findViewById(R.id.txtConecting);
        txtStatusConfirmation = findViewById(R.id.txtStatusConfirmation);
        txtClassroomConfirmation = findViewById(R.id.txtClassroomConfirmation);
        txtTimeConfirmation = findViewById(R.id.txtTimeConfirmation);


        txtClassroomConfirmation.setText(event.getStartDate());
        txtClassroomConfirmation.setText(event.getClassroom().getFullAddress());
        txtTimeConfirmation.setText(event.getStartDate());

        pgbConfirmation.setVisibility(View.VISIBLE);
        imgConfirmation.setVisibility(View.GONE);


    }

    public void activateServices() {

        RequirementsWizardFactory
                .createEstimoteRequirementsWizard()
                .fulfillRequirements(this,
                        new Function0<Unit>() {
                            @Override
                            public Unit invoke() {
                                Toast.makeText(getApplicationContext(), "Activando servicios", Toast.LENGTH_SHORT).show();
                                timer(15000, 1000);
                                return null;
                            }
                        },
                        new Function1<List<? extends Requirement>, Unit>() {
                            @Override
                            public Unit invoke(List<? extends Requirement> requirements) {
                                //Toast.makeText(getApplicationContext(), "Es necesario activar los servicios", Toast.LENGTH_SHORT).show();
                                Toast.makeText(getApplicationContext(), "No se han concedido los permisos necesarios", Toast.LENGTH_LONG).show();
                                return null;
                            }
                        },
                        new Function1<Throwable, Unit>() {
                            @Override
                            public Unit invoke(Throwable throwable) {
                                Log.e("app", "requirements error: " + throwable);
                                Toast.makeText(getApplicationContext(), "Se ha producido un error con los permisos de la app", Toast.LENGTH_LONG).show();
                                return null;
                            }
                        });

    }

    public void timer(final long totalTimeMilliseconds, long intervalMilliseconds) {

        new CountDownTimer(totalTimeMilliseconds, intervalMilliseconds) {
            int cont = 0;
            boolean activated = false;

            public void onTick(long millisUntilFinished) {

                if (!activated) {
                    initLocalitation();
                    activated = true;
                }

                if (cont > 3) {
                    cont = 0;
                }

                if (cont == 0) {
                    txtConecting.setText("Conectando");
                } else if (cont == 1) {
                    txtConecting.setText("Conectando.");
                } else if (cont == 2) {
                    txtConecting.setText("Conectando..");
                } else if (cont == 3) {
                    txtConecting.setText("Conectando...");
                }

                cont++;
            }

            public void onFinish() {
                if (!located) {
                    proximityContentManager.stop();
                    Toast.makeText(getApplicationContext(), "Activando comprobacion por GPS", Toast.LENGTH_SHORT).show();

                    if (checkPermission()) {
                        getGPSLocation();
                    } else {
                        Toast.makeText(getApplicationContext(), "Es necesario aceptar los permisos", Toast.LENGTH_LONG).show();

                        requestPermission();

                    }

                }

            }

        }.start();
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_FINE_LOCATION);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), INTERNET);

        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION, INTERNET}, PERMISSION_REQUEST_CODE);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (PERMISSION_REQUEST_CODE == requestCode) {
            if (grantResults.length > 0) {

                boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                boolean internetAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                if (locationAccepted && internetAccepted) {

                } else {

                    Toast.makeText(getApplicationContext(), "Acceso denegado", Toast.LENGTH_SHORT).show();

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                        if (shouldShowRequestPermissionRationale(ACCESS_FINE_LOCATION)) {

                            showMessageOKCancel("Es necesario aceptar los permisos",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(new String[]{ACCESS_FINE_LOCATION, INTERNET},
                                                        PERMISSION_REQUEST_CODE);
                                            }
                                        }
                                    });
                            return;
                        }
                    }

                }
            }

        }

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(getApplicationContext())
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    public void initLocalitation() {

        Log.d("APP", "/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/* 1: ");

        proximityContentManager = new ProximityContentManager(this, event);
        proximityContentManager.start(new VolleyCallback() {
            @Override
            public void onSuccess(Object object) {
                String value = (String) object;

                if (value.equals("true")) {
                    Log.d("APP", "/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/* Init location start result: " + value);
                    located = true;
                    proximityContentManager.stop();
                    sendConfirmation();

                }

            }
        });

    }

    public void sendConfirmation() {
        Log.d("APP", "/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/* 5: ");

        txtConecting.setVisibility(View.GONE);
        txtStatusConfirmation.setText("Asistencia confirmada");
        pgbConfirmation.setVisibility(View.GONE);
        imgConfirmation.setVisibility(View.VISIBLE);

        Log.d("APP", "/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/* 5.1: ");

        /*loadData.confirmParticipation(event.getFlavorCode(), event.getEventId(), new VolleyCallback() {
            @Override
            public void onSuccess(Object object) {

                Log.d("APP", ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Participacion de la peticion resultado " + object.toString());

                Intent intent = new Intent(getApplicationContext(), HomeView.class);
                startActivity(intent);
                finish();
            }
        });*/

        //loadData.comprobar(event.getFlavorCode(), event.getEventId());
        SendPost sendPost = new SendPost(getApplicationContext(), event.getFlavorCode(), event.getEventId());
        sendPost.execute();
        try {
            boolean response = (boolean) sendPost.get();

            if(response){
                Toast.makeText(getApplicationContext(), "Asistencia confirmada correctamente", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(), HomeView.class);
                startActivity(intent);
            }else{
                Toast.makeText(getApplicationContext(), "No se ha podido confirmar la asistencia", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(), HomeView.class);
                startActivity(intent);
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }


    public void getGPSLocation() {

        LocationManager locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);

        LocationListener locationListener = new LocationListener() {


            Location locationClassroom = new Location("Classroom");
            Location locationDevice = new Location("Device");

            public void onLocationChanged(Location location) {

                locationClassroom.setLatitude(Double.parseDouble(event.getClassroom().getLatitude()));
                locationClassroom.setLongitude(Double.parseDouble(event.getClassroom().getLongitude()));

                locationDevice.setLatitude(location.getLatitude());
                locationDevice.setLongitude(location.getLongitude());

                float distance = locationClassroom.distanceTo(locationDevice);

                if (distance < 5000) {

                    sendConfirmation();
                } else {
                    Toast.makeText(getApplicationContext(), "Estás fuera del rango permitido", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(getApplicationContext(), HomeView.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.putExtra("confirmation", true);
                    startActivity(intent);
                }

            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }
        };

        /*if (ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }*/

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 10, locationListener);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (proximityContentManager != null)
            proximityContentManager.stop();
    }

}
