package com.example.ioon015.amir_app.Vistas;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.ioon015.amir_app.Controladores.DataBaseController;
import com.example.ioon015.amir_app.Clases.SlideAdapter;
import com.example.ioon015.amir_app.R;

public class SlidesGuide extends AppCompatActivity {

    private ViewPager mSliderPager;
    private LinearLayout mDotLayout;

    private TextView[] mDots;

    private SlideAdapter slideAdapter;

    Button btnSkip;
    Button btnContinuar;

    private int mCurrentPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slides_guide);
        mSliderPager = findViewById(R.id.SlideViewPager);
        mDotLayout = findViewById(R.id.dotsLayout);

        btnSkip = findViewById(R.id.btn_skip);
        btnContinuar = findViewById(R.id.btnacceder);
        btnContinuar.setVisibility(View.GONE);

        slideAdapter = new SlideAdapter(this);

        mSliderPager.setAdapter(slideAdapter);

        addDotsIndicator(0);

        mSliderPager.addOnPageChangeListener(viewListener);

        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int current = getItem(+2);
                if(current<=slideAdapter.slide_images.length){
                    mSliderPager.setCurrentItem(current);
                }


            }
        });

        btnContinuar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DataBaseController dataBaseController = new DataBaseController(getApplicationContext());
                dataBaseController.notShowSlides(false);
                Intent intent = new Intent(getApplicationContext(), HomeView.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
    }

    public void addDotsIndicator(int position) {
        mDots = new TextView[slideAdapter.slide_images.length];
        int colorActive = getResources().getColor(R.color.colorAccent);
        int colorinactive = getResources().getColor(R.color.gray_normal);
        mDotLayout.removeAllViews();

        for (int i = 0; i < mDots.length; i++) {
            mDots[i] = new TextView(this);
            mDots[i].setText(Html.fromHtml("&#8226;"));
            mDots[i].setTextSize(35);
            mDots[i].setTextColor(getResources().getColor(R.color.gray_normal));

            mDotLayout.addView(mDots[i]);
        }
        if (mDots.length > 0) {
            mDots[position].setTextColor(getResources().getColor(R.color.colorAccent));
        }
    }


    private int getItem(int i) {
        return mSliderPager.getCurrentItem() + i;
    }


    ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            addDotsIndicator(position);

            mCurrentPage = position;

            if( position == slideAdapter.slide_images.length -1){
                btnSkip.setVisibility(View.INVISIBLE);
                btnContinuar.setVisibility(View.VISIBLE);

            } else {
                btnSkip.setVisibility(View.VISIBLE);
                btnContinuar.setVisibility(View.GONE);
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }


    };
}
