package com.example.ioon015.amir_app.Clases;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ioon015.amir_app.R;

import java.util.ArrayList;

public class GridAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Event> arrayList;

    public GridAdapter(Context context, ArrayList<Event> arrayList){

        this.context=context;
        this.arrayList=arrayList;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {


        if(convertView == null){
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.item_grid,null);
        }

        TextView txtClassName = convertView.findViewById(R.id.txtClassName);
        txtClassName.setText(arrayList.get(position).getFlavorCode());
        

        TextView txtClassTime = convertView.findViewById(R.id.txtClassTime);
        txtClassTime.setText(arrayList.get(position).getStartDate());

        TextView txtClassroomCode = convertView.findViewById(R.id.txtClassroomCode);
        txtClassroomCode.setText(arrayList.get(position).getClassroom().getCode());

        TextView txtClassStatus = convertView.findViewById(R.id.txtClassStatus);
        ImageView imgClock = convertView.findViewById(R.id.imgClockStatus);
        if(arrayList.get(position).getStatus()){
            txtClassStatus.setVisibility(View.VISIBLE);
            imgClock.setVisibility(View.VISIBLE);
        }else{
            txtClassStatus.setVisibility(View.GONE);
            imgClock.setVisibility(View.GONE);
        }



        return convertView;
    }
}