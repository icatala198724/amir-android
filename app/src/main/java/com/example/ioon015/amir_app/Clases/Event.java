package com.example.ioon015.amir_app.Clases;

import android.util.Log;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Event implements Serializable{


    private String flavorCode;
    private String eventId;
    private Classroom classroom;
    private String startDate;
    private String endDate;
    private String beacon;
    private boolean status;


    /**
     *
     * @param flavorCode    Indica el nombre de la asignatura
     * @param eventId       Indica el Id del evento
     * @param classroom     Indica la ubicación en el que tendrá lugar el evento
     * @param startDate     Es un objeto de la clase StartDate que recibe una fecha y hora de inicio y una fecha y hora de finalización de un evento
     * @param endDate       Indica la fecha y hora en la que da fin un evento
     * @param beacon        Indica el id del Beacon destinado en la ubicación del evento
     */
    public Event(String flavorCode, String eventId, Classroom classroom, String startDate, String endDate, String beacon) {
        this.flavorCode = flavorCode;
        this.eventId = eventId;
        this.classroom = classroom;
        this.startDate = startDate;
        this.endDate = endDate;
        this.beacon = beacon;
    }



    public boolean isEventNow(){

        boolean value = false;

        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm dd/MM/yyyy");

        try {
            Date dateEvent = formatter.parse(getStartDate());
            Calendar calendar = Calendar.getInstance();

            Date dateNow = calendar.getTime();

            if(dateNow.after(dateEvent)){

                calendar.setTime(formatter.parse(getStartDate()));

                /**************************************/
                /*Modificacion del tiempo para pruebas*/

                calendar.add(Calendar.MINUTE, 10);
                Date dateMaxTime  = calendar.getTime();
                if(dateNow.before(dateMaxTime)){
                    value = true;
                }
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return value;

    }

    public String getFlavorCode() {
        return flavorCode;
    }

    public void setFlavorCode(String flavorCode) {
        this.flavorCode = flavorCode;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public Classroom getClassroom() {
        return classroom;
    }

    public void setClassroom(Classroom classroom) {
        this.classroom = classroom;
    }

    public String getStartDate() {

        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm dd/MM/yyyy");

        String resultado = "";
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZZZZ");
        
        try {
            Date date = (Date) dateFormat.parse(startDate);
            resultado = formatter.format(date.getTime());

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return resultado;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getBeacon() {
        return beacon;
    }

    public void setBeacon(String beacon) {
        this.beacon = beacon;
    }

    public boolean getStatus() {



        try {
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat format = new SimpleDateFormat("HH:mm dd/MM/yyyy");
            //String dateFormated = format.parse(getStartDate());

            Date dateNow = calendar.getTime();

            Date test = format.parse(getStartDate());

            calendar.setTime(test);

            Date dateClass = calendar.getTime();

            if(dateNow.equals(dateClass)){
                setStatus(true);
            }else{
                setStatus(false);
            }


        } catch (ParseException e) {
            e.printStackTrace();
        }


        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }


}
