package com.example.ioon015.amir_app.Clases;

public class Classroom {

    private String code;
    private String description;
    private String fullAddress;
    private String zone;
    private String latitude;
    private String longitude;

    /**
     *
     * @param code          Indica el código de la ubicación del evento
     * @param description   Indica la descripción del evento
     * @param fullAddress   Indica la dirección del evento
     * @param zone          Indica la zona del evento
     * @param latitude      Indica la latitud del evento
     * @param longitude     Indica la longitud del evento
     */
    public Classroom(String code, String description, String fullAddress, String zone, String latitude, String longitude) {
        this.code = code;
        this.description = description;
        this.fullAddress = fullAddress;
        this.zone = zone;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
