package com.example.ioon015.amir_app.Vistas;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ioon015.amir_app.Controladores.VolleyCallback;
import com.example.ioon015.amir_app.Peticiones.LoadData;
import com.example.ioon015.amir_app.R;

public class Notifications extends Fragment {



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        LoadData ld = new LoadData(getContext());
        ld.listClassroom(new VolleyCallback() {
            @Override
            public void onSuccess(Object object) {

                Log.d("APP", ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> SUCESS!");
            }
        });


        return inflater.inflate(R.layout.activity_notifications, null);
    }
}
