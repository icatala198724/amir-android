package com.example.ioon015.amir_app.Clases;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.ioon015.amir_app.R;


public class SlideAdapter extends PagerAdapter{
    Context context;
    LayoutInflater inflater;

    public SlideAdapter(Context context) {
        this.context=context;
    }

    public int[] slide_images = {
            R.drawable.slide0,
            R.drawable.slide1,
            R.drawable.slide2
    };



    @Override
    public int getCount() {
        return slide_images.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        String[] slide_text = {
                context.getString(R.string.slide_welcome),
                context.getString(R.string.slide_classes),
                "¿Estas listo? \n ¡Adelante!"
        };


        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.slide_layout,container,false);

        ImageView slideImageview = view.findViewById(R.id.slide_image);
        TextView slideText = view.findViewById(R.id.slide_text);


        slideImageview.setImageResource(slide_images[position]);
        slideText.setText(slide_text[position]);

        container.addView(view);

        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((ConstraintLayout)object);
    }
}
