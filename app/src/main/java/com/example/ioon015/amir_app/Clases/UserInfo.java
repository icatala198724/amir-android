package com.example.ioon015.amir_app.Clases;

public class UserInfo {

    FlavourTable flavor;
    UserTable user;

    public UserInfo() {
    }

    public FlavourTable getFlavor() {
        return flavor;
    }

    public void setFlavor(FlavourTable flavor) {
        this.flavor = flavor;
    }

    public UserTable getUser() {
        return user;
    }

    public void setUser(UserTable user) {
        this.user = user;
    }
}
