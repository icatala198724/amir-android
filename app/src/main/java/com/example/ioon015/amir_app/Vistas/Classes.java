package com.example.ioon015.amir_app.Vistas;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ioon015.amir_app.Clases.Classroom;
import com.example.ioon015.amir_app.Clases.Event;
import com.example.ioon015.amir_app.Clases.GridAdapter;
import com.example.ioon015.amir_app.Controladores.VolleyCallback;
import com.example.ioon015.amir_app.Peticiones.LoadData;
import com.example.ioon015.amir_app.R;

import java.io.Serializable;
import java.util.ArrayList;


public class Classes extends Fragment {

    View view;
    private GridView gridView;
    private GridAdapter adapter;

    private LoadData loadData;
    private Event[] events;
    private Switch switchDaysClasses;
    private boolean allEvents = false;
    private FrameLayout gridLayout;

    private static final String EVENT_IN_TIME = "EVENT_IN_TIME";
    private HomeView homeView = new HomeView();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.activity_classes, container, false);
        loadData = new LoadData(getContext());

        gridLayout = view.findViewById(R.id.layoutGrid);

        gridView = view.findViewById(R.id.gridViewClasses);
        switchDaysClasses = view.findViewById(R.id.switchDaysClasses);
        final TextView txtEventsCounter = view.findViewById(R.id.txtActualClassroom);

        gridLayout.setVisibility(View.VISIBLE);


        loadData.nextEvents(allEvents, new VolleyCallback() {
            @Override
            public void onSuccess(Object object) {

                if(object.toString().equals("false")){
                    gridLayout.setVisibility(View.GONE);
                }else{

                    events = (Event[]) object;
                    final ArrayList<Event> arrayList = new ArrayList<Event>();

                    if(events.length > 0){
                        for (Event event: events) {
                            arrayList.add(event);
                        }
                    }

                    adapter = new GridAdapter(getContext(), arrayList);
                    gridView.setAdapter(adapter);
                    txtEventsCounter.setText(adapter.getCount() + " Total");
                    gridLayout.setVisibility(View.GONE);

                    if(arrayList.size() > 0){

                        final Event event = arrayList.get(0);
                        boolean value = event.isEventNow();

                        if(value){

                            try{

                                boolean confirmation = getActivity().getIntent().getBooleanExtra("confirmation", false);

                                if(!confirmation){
                                    Log.d("APP", ".................................................. Clase no confirmada");
                                    loadData.checkParticipation(event.getFlavorCode(), event.getEventId(), new VolleyCallback() {
                                        @Override
                                        public void onSuccess(Object object) {

                                            if(object.toString().equals("false")){
                                                //homeView.startBeaconConfirmation(event);
                                                Bundle bundle = new Bundle();
                                                //bundle.putSerializable(EVENT_IN_TIME, event);

                                                //Toast.makeText(getApplicationContext(), "Tienes una clase en hora", Toast.LENGTH_SHORT).show();
                                                Intent intent = new Intent(getContext(), PresenceConfirmation.class);
                                                intent.putExtra("EventValue0", event.getFlavorCode());
                                                intent.putExtra("EventValue1", event.getEventId());
                                                intent.putExtra("EventValue2", event.getClassroom().getFullAddress());
                                                intent.putExtra("Latitude", event.getClassroom().getLatitude());
                                                intent.putExtra("Longitude", event.getClassroom().getLongitude());
                                                intent.putExtra("EventValue3", event.getStartDate());
                                                intent.putExtra("EventValue4", event.getEndDate());
                                                intent.putExtra("EventValue5", event.getBeacon());
                                                intent.putExtra("EventValue6", event.getStatus());

                                                getActivity().startActivity(intent);
                                            }

                                        }
                                    });
                                }

                            }catch (Exception e){

                            }


                        }
                    }

                }


            }
        });

        switchDaysClasses.setChecked(true);

        switchDaysClasses.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked){
                    allEvents = false;
                }else{
                    allEvents = true;
                }

                gridLayout.setVisibility(View.VISIBLE);

                loadData.nextEvents(allEvents, new VolleyCallback() {
                    @Override
                    public void onSuccess(Object object) {

                        if(object.toString().equals("false")){
                            gridLayout.setVisibility(View.GONE);
                        }else{

                            events = (Event[]) object;
                            final ArrayList<Event> arrayList = new ArrayList<Event>();

                            if(events.length > 0){
                                for (Event event: events) {
                                    arrayList.add(event);
                                }
                            }

                            adapter = new GridAdapter(getContext(), arrayList);
                            gridView.setAdapter(adapter);
                            txtEventsCounter.setText(adapter.getCount() + " Total");
                            gridLayout.setVisibility(View.GONE);

                        }

                    }
                });

            }
        });

        return view;
    }

}
