package com.example.ioon015.amir_app.Clases;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import com.example.ioon015.amir_app.Controladores.ConexionSQLiteHelper;

import java.util.ArrayList;

public class SwipeDelete {
    ArrayList<Notificacion> arrayListNotifications;
    AdapterList adap;
    Context context;
    RecyclerView rv;

    private void loadSwipe() {
        final ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                arrayListNotifications.remove(viewHolder.getAdapterPosition());
                ConexionSQLiteHelper conn = new ConexionSQLiteHelper(context);
                int idRemove = Integer.valueOf(viewHolder.itemView.getTag().toString());
                conn.removeItem(idRemove);
                adap.notifyDataSetChanged();
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                Paint color = new Paint();

                View itemview = viewHolder.itemView;


                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {

                    if (dX > 0) {

                        color.setColor(Color.parseColor("#FF0066"));
                        RectF fondo = new RectF((float) itemview.getLeft(), (float) itemview.getTop(), dX, (float) itemview.getBottom());
                        c.drawRect(fondo, color);


                    } else {
                        color.setColor(Color.parseColor("#FF0066"));
                        RectF fondo = new RectF((float) itemview.getRight() + dX, (float) itemview.getTop(), itemview.getRight(), (float) itemview.getBottom());
                        c.drawRect(fondo, color);


                    }
                }

                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };


        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(rv);

    }
}
