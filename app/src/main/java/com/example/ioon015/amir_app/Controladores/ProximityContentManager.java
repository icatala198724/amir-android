package com.example.ioon015.amir_app.Controladores;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Credentials;
import android.os.CountDownTimer;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.estimote.proximity_sdk.proximity.EstimoteCloudCredentials;
import com.estimote.proximity_sdk.proximity.ProximityContext;
import com.estimote.proximity_sdk.proximity.ProximityObserver;
import com.estimote.proximity_sdk.proximity.ProximityObserverBuilder;
import com.estimote.proximity_sdk.proximity.ProximityZone;
import com.example.ioon015.amir_app.Clases.BeaconData;
import com.example.ioon015.amir_app.Clases.Event;

import java.util.ArrayList;
import java.util.List;

import kotlin.Unit;
import kotlin.jvm.functions.Function1;

public class ProximityContentManager extends Activity{

    private Context context;

    private ProximityObserver proximityObserver;
    private ProximityObserver.Handler proximityObserverHandler;
    private String beaconId = "";
    private com.example.ioon015.amir_app.Clases.Credentials credentials = new com.example.ioon015.amir_app.Clases.Credentials();
    private Event event;

    public ProximityContentManager(Context context, Event event) {
        this.context = context;
        this.event = event;
    }


    public void start(final VolleyCallback volleyCallback){

        proximityObserver = new ProximityObserverBuilder(context, credentials.getCloudCredentials()).withLowLatencyPowerMode().withOnErrorAction(new Function1<Throwable, Unit>() {
            @Override
            public Unit invoke(Throwable throwable) {

                /*if (!throwable.toString().contains("com.estimote.proximity_sdk.exception.UnableToDetectMonitoringStateException")){
                    Log.d("APP", "............................................................. Error de la localizacion: " + throwable);
                    volleyCallback.onSuccess("false");
                }*/

                volleyCallback.onSuccess("false");
                return null;
            }
        }).build();


        ProximityZone proximityZoneMin = this.proximityObserver.zoneBuilder().forTag("mesa").inCustomRange(0.2).create();

        ProximityZone proximityZoneMax = this.proximityObserver.zoneBuilder().forTag("mesa").inCustomRange(5.5).create();

        ProximityZone zone = this.proximityObserver.zoneBuilder().forTag("mesa").inFarRange().withOnEnterAction(new Function1<ProximityContext, Unit>() {
            @Override
            public Unit invoke(ProximityContext proximityContext) {

                beaconId = proximityContext.getAttachments().get("beacon");


                if(beaconId.equals(event.getBeacon())){
                    Log.d("APP", "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Beacon unico: " + beaconId);
                    volleyCallback.onSuccess("true");

                }

                return null;
            }
        }).create();

        this.proximityObserver.addProximityZone(zone);
        //this.proximityObserver.addProximityZone(proximityZoneMin);
        //this.proximityObserver.addProximityZone(proximityZoneMax);

        proximityObserver.start();
        proximityObserverHandler = proximityObserver.start();
    }

    public void stop(){
        proximityObserverHandler.stop();
    }

}
