package com.example.ioon015.amir_app.Clases;

public class UserTable{
    public static final String TABLE_NAME = "user";
    public static final String NAME = "name";
    public static final String SURNAME = "surname";
    public static final String USER_NAME = "userName";
    public static final String EMAIL = "email";
    public static final String ROLE = "role";
    public static final String CATEGORY = "category";
    public static final String ACTIVE = "active";
    public static final String ID_CURSO = "idCurso";
    public static final String ID_COUNTTRY = "idCountry";


    String name;
    String surname;
    String userName;
    String email;
    String role;
    String category;
    String active;
    String idCurso;
    String idCountry;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getIdCurso() {
        return idCurso;
    }

    public void setIdCurso(String idCurso) {
        this.idCurso = idCurso;
    }

    public String getIdCountry() {
        return idCountry;
    }

    public void setIdCountry(String idCountry) {
        this.idCountry = idCountry;
    }
}