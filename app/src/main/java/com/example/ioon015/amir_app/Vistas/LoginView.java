package com.example.ioon015.amir_app.Vistas;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ioon015.amir_app.Controladores.DataBaseController;
import com.example.ioon015.amir_app.Controladores.VolleyCallback;
import com.example.ioon015.amir_app.Peticiones.LoadData;
import com.example.ioon015.amir_app.R;

public class LoginView extends AppCompatActivity {

    TextView txtUsername;
    TextView txtPassword;

    Button btnLogin;
    Button btnForgotPass;

    LoadData loadData;

    DataBaseController dataBaseController;

    FrameLayout frameLayout;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_view);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        frameLayout= findViewById(R.id.frameLayoutProgress);
        progressBar = findViewById(R.id.progressBarLoading);
        frameLayout.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);


        loadData = new LoadData(this);
        dataBaseController = new DataBaseController(this);

        btnLogin = findViewById(R.id.btnLogin);
        btnForgotPass = findViewById(R.id.btnForgotPass);

        txtUsername = findViewById(R.id.txtRecoveryUser);
        txtPassword = findViewById(R.id.txtPass);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String user = txtUsername.getText().toString();
                final String pass = txtPassword.getText().toString();

                if(user.equals("") || pass.equals("")){
                    Toast.makeText(getApplicationContext(), "Rellene todos los campos" , Toast.LENGTH_SHORT).show();
                }else{

                    frameLayout.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.setIndeterminate(true);

                    txtUsername.setEnabled(false);
                    txtPassword.setEnabled(false);
                    btnLogin.setEnabled(false);

                    loadData.login(user, pass, new VolleyCallback() {
                        @Override
                        public void onSuccess(Object object) {
                            boolean result = (boolean) object;

                            Log.d("APP", "------------------------------------------- Resultado consulta slides 1");

                            frameLayout.setVisibility(View.GONE);
                            progressBar.setVisibility(View.GONE);
                            progressBar.setIndeterminate(false);

                            txtUsername.setEnabled(true);
                            txtPassword.setEnabled(true);
                            btnLogin.setEnabled(true);

                            if (result){

                                DataBaseController dataBaseController = new DataBaseController(getApplicationContext());
                                dataBaseController.saveLoginData(user, pass);

                                boolean show = dataBaseController.showSlides();

                                Log.d("APP", "------------------------------------------- Resultado consulta slides: " + show);

                                if(!show){
                                    Intent intent = new Intent(getApplicationContext(), HomeView.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                }else{
                                    Intent intent = new Intent(getApplicationContext(), SlidesGuide.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                }


                            }else{
                                Toast.makeText(getApplicationContext(), "El usuario/contraseña son incorrectos" , Toast.LENGTH_LONG).show();
                            }

                        }
                    });

                }

            }
        });

        btnForgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), RecoveryPassView.class);
                startActivity(intent);
            }
        });



    }

}
